# Devops Test

Our test has three different topics which are:

1. Architecture & process definition (~1h30)
2. Kubernetes use cases (~2h)
3. Technical questions (~30min)

The whole test should roughly take 4 hours to complete.

# 1. Architecture & Process

- We should have observability

> To have observability, I would first consider using open-source tools such as:
 - *Jaeger* to gain insights and be able to troubleshoot distributed micro-services by analyzing traces.
 - *Fluentd* or *Logstash* to collect logs.
 - *Prometheus* to be able to store the data and query it. And also to be able to get alerts based on thresholds using the *Alertmanager* component.
 - *Grafana* to visualize data in dashboards.

> And if the budget allows it, I would also consider using a paid solution such as Datadog.

> Note that instead of using Prometheus and Grafana, we could use ElasticSearch and Kibana. But Prometheus and Grafana seem to be better designed to work with Kubernetes based applications.


- Access to Kubernetes components must be secured. Some users have full access whereas some others may have limited access.

> To secure the Kubernetes components, I would make use of the Kubernetes built-in role-based access control (RBAC). I would give specific and fine-grained access based on each needs following the principle of least privilege because people should only be given access to the resources that they need. For example, a cluster administrator or devops, could be granted a *cluster-admin* ClusterRole which allows superuser operations in all of the cluster resources. And developers could be given read access to specific API objects such as Pods or Deployments but not to ConfigMaps or Secrets which are more sensitive. Access could also be given based on the team or the internal department (e.g. the data engineering team or the frontend team) and such isolation could be done using dedicated Namespaces. To test and make sure that RBAC policies are working as they should, the cluster administrator can use  the command `kubectl auth can-i xxx yyy --as=zzz` to make sure that the user *zzz* can perform the action *xxx* on the resource *yyy*. Or the cluster administrator can use a more extensive tool such as [rakkess](https://github.com/corneliusweig/rakkess).


- Our Kubernetes cluster should be fault tolerant, in the sense that a network outage in one cloud zone / region should not impact much our applications’ up-time

> For the cluster to be fault tolerant, I would create a *Multi-zonal Cluster* or a *Regional Cluster*. In a Multi-zonal cluster, the nodes are running in multiples zones, so if a zone goes down, the workload is still up. But in this case the control plane is only available in a single zone and if this zone goes down the cluster would not be configurable during the outage.

> An alternative would be to use a Regional Cluster. In this case, the control plane is replicated in multiple zones within a single region and nodes are replicated in each of these zones. This alternative is more resilient but it consumes more Compute Engine resources and is therefore more costly.

> Note that Google has an internal goal to keep the monthly uptime percentage at 99.5% for the Kubernetes API server for zonal clusters and 99.95% for regional clusters.


- Our Kubernetes cluster nodes should not have a public IP address. All incoming traffic should by handled by a Load-balancer

> To achieve this, I would choose to create a [Private Cluster](https://cloud.google.com/kubernetes-engine/docs/concepts/private-cluster-concept) and make sure that the public endpoint access is disabled which means that it prevents all internet access to both masters and nodes. End-users will still be able to reach internal services if they are exposed with a Service of type LoadBalancer by reaching the IP of the LoadBalancer.


- Some of our applications should be deployed in a separate subnet

> I would create a new subnet inside the VPC or create another VPC and peer the two VPCs so that the subnet is reachable. I would then taint and label the nodes inside this subnet and add the corresponding tolerations to the application's Deployment so that only specified applications should run onto this subnet.


- Developers’ desktop environment should stick as close as possible to production

> I would suggest having a staging cluster mimicking production where developers could experiment and test how their code interacts with the full architecture instead of trying to run it locally on their machines.


- We must have a CI/CD

> Depending on the application, I would set up a pipeline to automatically build, test and deploy the code to a staging cluster. The pipeline could trigger on each reviewed and approved merge request. The stage to deploy to production could also be handled by the CI/CD pipeline but could require a manual action to be triggered. Great CI/CD tools are Gitlab CI/CD, CircleCI or Jenkins among other.


- We need an efficient backup strategy for data related services

> To back up data related services, I would create Kubernetes CronJobs to create daily/weekly/monthly backups and store those backups in Cloud Storage.



Besides the building block of the infrastructure, we’d like to have information on how you would handle the following topics:


- How would you handle application configuration ?

> I would not configure the applications directly from within the Dockerfile to avoid incomprehensible files. I would rather use another tool such as Ansible or an equivalent tool instead. There would be in this case an Ansible playbook to configure each application. However, Ansible has no notion of state and this could be problematic for tracking purposes. An alternative such a Puppet could be used instead.


- What processes and tools do you plan to use in order to have a proper "Infrastructure As Code" approach

> I would use the tool Terraform for that purpose. Terraform integrates nicely with public cloud providers such as Google. Every bit of the architecture should be described in Terraform code and no change to the architecture should be done manually. Before each change, the result of the Terraform plan should be carefully reviewed and should be versioned in git to keep track of all the changes.


- Would you recommend a full cloud approach ? Hybrid deployment ?

> I would recommend a full cloud approach to stick with the same kind of configuration management of the infrastructure (e.g. Terraform with Google Cloud). Even though an hybrid approach where the standard load is handled by an on-premise solution and only the load pikes are handled by a public cloud provider could be a cost-effective solution. It would require much more efforts in terms of configuration and would require more man-power in my opinion.


- Even though we’re not Netflix, what’s your opinion on chaos engineering ? Would you recommend it and, if so, what should be tested and how ?

> Not enough time for this question.


- How would you deploy stateful applications such as databases ? Would you have them within Kubernetes and if so how would you manage them ?

> Not enough time for this question.

---

# 2. Kubernetes

You will have to deploy a small Node.js app on Kubernetes from scratch.

The application soure code and instuctions to build and run it are available in the README file at https://github.com/oui-team/devops-interview/app.

What you need to produce:

- A dockerfile to build the application
- A manifest to deploy the app (you may pick your own: Kubernetes, Helm, …)
- A description of the components you would use to monitor the application (see the bonus part to include it in your manifest)
- A precise documentation of every component you will use
- A description of the complete CI/CD tooling you will use to deploy
- A successful and working deployment on Kubernetes
- An ingress and service that route incoming traffic to our application

We provide an external static IP address. We will send you the IP address and a domain name resolving to this IP.

We will pay extra attention to:

- Your Dockerfile

> The Dockerfile is available at app/Dockerfile

- How you handle configuration


- Your choices regarding deployment tools and monitoring

> A small Gitlab CI/CD pipeline is used to build the Docker image and push it to Docker Hub. The pipeline definition is available in the file .gitlab-ci.yml .

> I tried to deploy Prometheus and Grafana using Helm but the cluster did not have enough CPU for Prometheus. So I removed both services.

- Your manifests formats (helm, terraform, raw, ..)

> My manifests are available in the directory manifests/ and are raw.

- The scaling capabilities of your deployment

A Kubernetes cluster is available to deploy your app. You may use the public Docker Hub to publish your image.

## 2.1 Bonus

Once deployed, we should be able to monitor the application: basic memory/CPU usage, HTTP codes, eventual errors…
So if you still have time:

- The deployed ingress should only allow whitelisted IP addresses
- Add a monitoring service in your manifest (like Grafana) and connect it
- Secure your app with a certificate (you can use LetsEncrypt). A letsencrypt issuer is already deployed, its name is `letsencrypt-prod`

> I managed to secure the app by provisioning the LoadBalancer with the certificate issued by `letsencrypt-prod`. 

---

# 3. Technical Questions (bonus)


1. Imagine that one of your colleagues accidentally applied all the “production” config-maps to the “beta” environment.
    -  What impacts could this have ?
    - How would you
        - Get things running properly again ?
        - Find what has been impacted ?
        - Prevent a re-occurrence of the issue ?
2. There is a large amount of non-public data stored in a MySQL database located on a virtual machine that needs to be accessed by typical well-maintained and tested APIs as well as by data engineers in “exploratory” mode (they don’t know which data they need until they need it). This VM is mounted on a RAID 4 disk system and the disk space is running low.
Describe, with diagrams if needed, the systems and checks you would implement to ensure the data can be accessed by all parties in a secure manner. What would you do about the disk space ?

Be sure to detail the pros and cons of your answers

> Not enough time for this section.
